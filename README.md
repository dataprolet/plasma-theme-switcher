# Plasma Theme Switcher

Moved to https://git.dataprolet.de/dataprolet/Plasma-Theme-Switcher.

## Description
This is a simple script that changes the Plasma Breeze theme according to a custom time or according to your location's sunrise/sunset using a systemd timer.

The default is setting the theme to dark at 9:30 p.m. and back to light at 6:30 a.m.

## Installation

### Use the installer.sh script to set up automatically
```
$ git clone https://gitlab.com/dataprolet/plasma-theme-switcher
$ cd plasma-theme-switcher
$ sh installer.sh
```

### Optional:
- Change the service file to specify which script to use.
- Change either script to a custom time or custom location.

### Install manually
1. Place the timer and service file in the user directory `~/.config/systemd/use`
2. Place the script into a directory and specify the path in the service file
3. Change the time in the script if necessary (the default is 9:30 p.m. and 6:00 a.m.)
4. Alternatively use the `theme-switcher-sun.sh` script to change the theme according to your location's sunrise/sunset and specify a location in the script
5. Enable and start the timer:  
```
$ systemctl --user enable theme-switcher.timer  
$ systemctl --user start theme-switcher.timer
```
