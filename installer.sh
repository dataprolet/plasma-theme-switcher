#/bin/bash

# Create systemd user directory
mkdir -p ~/.config/systemd/user

# Copy unit files
cp theme-switcher.{service,timer} ~/.config/systemd/user

# Add username to service file
sed -i "s/username/$(whoami)/g" ~/.config/systemd/user/theme-switcher.service

# Make directory for scripts
mkdir -p ~/.theme-switcher

# Copy script file
cp *.sh ~/.theme-switcher

# Enable and start timer
systemctl --user enable theme-switcher.timer
systemctl --user start theme-switcher.timer
systemctl --user start theme-switcher